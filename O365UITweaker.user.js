// ==UserScript==
// @name         O365UITweaker
// @namespace    https://bitbucket.org/achernyakevich/tampermonkey-scripts/
// @version      0.1.0
// @description  This script tweaks some UI elements of O365 (like header color, etc.)
// @author       Alexander Chernyakevich <tch@rambler.ru>
// @match        https://outlook.office.com/*
// @noframes
// @run-at       document-idle
// @grant        GM_log
// ==/UserScript==

(function() {
    'use strict';

    setTimeout(function(){
        let sheet = document.createElement('style');
        sheet.innerHTML =
            "div#O365_NavHeader {background: var(--headerButtonsBackground)}\n" +
            ".o365cs-base .o365sx-appName {background: var(--headerButtonsBackground)}\n" +
            ".o365cs-base .o365sx-appName:hover, .o365cs-base .o365sx-appName:focus {background: var(--headerButtonsBackgroundHover)}\n" +
            ".o365cs-base .o365sx-button {background: var(--headerButtonsBackground)}\n" +
            ".o365cs-base .o365sx-button:hover, .o365cs-base .o365sx-button:focus {background: var(--headerButtonsBackgroundHover)}";
        document.body.appendChild(sheet);
        GM_log("o365 tweak styles added.");
    }, 3000);

})();
