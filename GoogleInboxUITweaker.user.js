// ==UserScript==
// @name         GoogleInboxUITweaker
// @namespace    https://bitbucket.org/achernyakevich/tampermonkey-scripts/
// @version      0.2.1
// @description  This script tweaks some UI elements of Google Inbox (like mail text font size presentation, etc.)
// @author       Alexander Chernyakevich <tch@rambler.ru>
// @match        https://inbox.google.com/*
// @noframes
// @run-at       document-idle
// @grant        GM_log
// @grant        GM_registerMenuCommand
// @grant        GM_unregisterMenuCommand
// @grant        GM_getValue
// @grant        GM_setValue
// @grant        GM_notification
// ==/UserScript==

(function() {
    'use strict';

    var lastStyleSheet = document.styleSheets[document.styleSheets.length-1];
    lastStyleSheet.insertRule("div.n7 {font-size: medium}", lastStyleSheet.cssRules.length);
    lastStyleSheet.insertRule("div.gmail_msg {font-size: medium}", lastStyleSheet.cssRules.length);

    // CONFIGURE: Notifications disaplying time (before it will be hidden), milliseconds
    var notificationDisplayingDelay = 5000;
    // CONFIGURE: Warnings disaplying time (before it will be hidden), milliseconds
    var warningDisplayingDelay = 5000;
    // CONFIGURE: Line below configure mail checking notifcation period in *minutes*
    var checkMailMinutes = 10;

    var intervalObject, checkMailInterval = checkMailMinutes*60*1000; // m * s * ms
    var toggleMenuItem;
    var tabVisibility = "disabled";
    var lastMailsCountInInox;
    document.addEventListener("visibilitychange", onVisibilityChange);

    function onVisibilityChange(evt) {
        if ( tabVisibility !== "disabled" ) {
            if ( getActiveFolderName() === "INBOX" ) {
                if ( tabVisibility ) {
                    lastMailsCountInInox = getNumberOfMailsInInbox();

                    intervalObject = setInterval(function() {
                        var newMailsCountInInbox = getNumberOfMailsInInbox() - lastMailsCountInInox;
                        if ( newMailsCountInInbox !== 0) {
                            showNewMailNotification(newMailsCountInInbox);
                        }
                    }, checkMailInterval);
                } else {
                    clearInterval(intervalObject);
                }
            } else {
                if ( tabVisibility ) {
                    showNotInInboxWarning();
                }
            }
            tabVisibility = !tabVisibility;
        }
    }
    function showNewMailNotification(count) {
        showNotification("New mails arrived (" + count + ").");
    }
    function showNotInInboxWarning() {
        showNotification("Select INBOX to get notifications about new mails arrival.",
                         "WARNING: Google Inbox",
                         "https://cdn.pixabay.com/photo/2013/07/12/11/59/warning-145066_1280.png",
                         warningDisplayingDelay);
    }
    function showNotification(text, title, image, timeout) {
        GM_notification({
            title: ( title ? title : "Google Inbox" ),
            text: text,
            image: ( image ? image : "https://ssl.gstatic.com/bt/C3341AA7A1A076756462EE2E5CD71C11/ic_product_inbox_16dp_r2_2x.png"),
            highlight: true,
            timeout: ( timeout ? timeout : notificationDisplayingDelay)
        });
    }
    function getDivElementByRole(from, condition) {
        var element = null;
        var divs = from.getElementsByTagName("div");
        for (var i=0; i < divs.length; i++) {
            element = divs[i];
            if ( condition(element) ) {
                break;
            }
        }
        return element;
    }
    function findTodayDiv() {
        var appDiv = getDivElementByRole(document, function(element) {
            return ( element.attributes.role && element.attributes.role.value === "application" );
        });

        var d = new Date(); d.setMilliseconds(0); d.setSeconds(0); d.setMinutes(0); d.setMinutes(0); d.setHours(0);
        var idTP = d.getTime();
        var todayDiv = getDivElementByRole(appDiv, function(element) {
            return ( element.attributes.jsinstance && element.attributes.jsinstance.value.indexOf(";"+idTP) >= 0 );
        });

        return todayDiv;
    }
    function getNumberOfMailsInInbox() {
        var todayDiv = findTodayDiv();
        return ( todayDiv ? todayDiv.children[1].childElementCount : 0 );
    }
    function getActiveFolderName() {
        var loc = window.location.href;
        return ( loc.substr(loc.length-1) === "/" || loc.indexOf("/?pli=1") > 0 ? "INBOX" : "not inbox" );
    }

    function toggleNotification() {
        if ( toggleMenuItem ) {
            GM_unregisterMenuCommand(toggleMenuItem);
        } else {
            tabVisibility = true;
        }
        if ( tabVisibility === "disabled" ) {
            tabVisibility = true;
            toggleMenuItem = GM_registerMenuCommand("Turn OFF new mails notifications", toggleNotification);
            showNotification("New mails notifications have been turned ON.");
        } else {
            tabVisibility = "disabled";
            toggleMenuItem = GM_registerMenuCommand("Turn ON new mails notifications", toggleNotification);
            showNotification("New mails notifications have been turned OFF.");
        }
    }
    toggleNotification();

})();
