// ==UserScript==
// @name         Autocompleter
// @namespace    https://bitbucket.org/achernyakevich/tampermonkey-scripts/
// @version      0.5.4
// @description  This script brings possibility to automcomplete text by abbreviation followed by Ctrl+SPACE keypress.
// @author       Alexander Chernyakevich <tch@rambler.ru>
// @match        https://github.com/*/issues/*
// @include      /^https?:\/\/(\w|-)+\.(\w|-)+\.(net|com)\/browse\/.*/
// @include      /^https?:\/\/.+\..+\/issues\/\d+([#\?].*)?$/
// @match        https://reports.scand.by/default.php*
// @match        https://reports.scand.by/addreportuser.php*
// @match        https://reports.scand.by/addreport.php*
// @match        https://reports.scand.by/flamingo-ui/*
// @grant        GM_log
// @grant        GM_setValue
// @grant        GM_getValue
// @grant        GM_registerMenuCommand
// @grant        GM_listValues
// @require      https://bitbucket.org/achernyakevich/tampermonkey-scripts/raw/configHelper-0.1.1/common/configHelper.js
// ==/UserScript==
(function() {
    'use strict';

    const GITHUB_ISSUES_LASTLOADED_URL = "github.issues.lastLoaded.url";
    const CLOUDJIRA_ISSUES_LASTLOADED_URL = "cloudjira.issues.lastLoaded.url";
    const CLOUDJIRA_ISSUE_URL_PATTERN = "^https?:\\/\\/(\\w|-)+\\.(\\w|-)+\\.(net|com)\\/browse\\/";
    const REDMINE_ISSUES_LASTLOADED_URL = "redmine.issues.lastLoaded.url";
    const REDMINE_ISSUE_URL_PATTERN = "^(https?:\\/\\/)?([^\\/]+)\\/issues\\/(\\d+)([#\\?]?.*)?$";
    const GITLAB_ISSUES_LASTLOADED_URL = "gitlab.issues.lastLoaded.url";
    const GITLAB_ISSUE_URL_PATTERN = "^(https?:\\/\\/)?([^\\/]+\\/)+-\\/issues\\/(\\d+)([#\\?]?.*)?$";

    function expandShortcut(sElement) {
        let val = sElement.value;
        let spaceIdx = val.lastIndexOf(" ");
        let shortcut = val.substring(spaceIdx+1).toLowerCase();
        if ( shortcut.length > 0 && myAutoCompletionList[shortcut] ) {
            sElement.value = val.substr(0, spaceIdx+1) + myAutoCompletionList[shortcut]();
        }
    }
    function expandGitHubIssueId(prefix) {
        let ghIssueUrl = GM_getValue(GITHUB_ISSUES_LASTLOADED_URL, "");

        if ( ghIssueUrl == "" ) {
            alert("GitHub issues information is not available (probably you didn't open any).");
            return prefix;
        } else {
            return ghIssueUrl.replace("https://github.com/", prefix) + " ";
        }
    }
    function expandCloudJiraIssueId(prefix) {
        let jiraIssueUrl = GM_getValue(CLOUDJIRA_ISSUES_LASTLOADED_URL, "");

        if ( jiraIssueUrl == "" ) {
            alert("Cloud Jira issues information is not available (probably you didn't open any).");
            return prefix;
        } else {
            return "(" + jiraIssueUrl.replace(new RegExp(CLOUDJIRA_ISSUE_URL_PATTERN), '') + ") ";
        }
    }
    function expandRedmineIssueId(prefix) {
        let redmineIssueUrl = GM_getValue(REDMINE_ISSUES_LASTLOADED_URL, "");

        if ( redmineIssueUrl == "" ) {
            alert("Redmine issues information is not available (probably you didn't open any).");
            return prefix;
        } else {
            return "#" + redmineIssueUrl.match(new RegExp(REDMINE_ISSUE_URL_PATTERN))[3] + " ";
        }
    }
    function expandGitLabIssueId(prefix) {
        let gitlabIssueUrl = GM_getValue(GITLAB_ISSUES_LASTLOADED_URL, "");

        if ( gitlabIssueUrl == "" ) {
            alert("GitLab issues information is not available (probably you didn't open any).");
            return prefix;
        } else {
            return "#" + gitlabIssueUrl.match(new RegExp(GITLAB_ISSUE_URL_PATTERN))[3] + " ";
        }
    }

    function customizeAutoCompletionList(list) {
        // sample:
        // {"list" : {"woi" : "Working on internal issues.", "meet" : "Meeting with "}}
        let configStr = GM_getValue('autocompleter.config');
        let config = configHelper.getConfigObject(configStr);
        if ( config != null && config.list != null ) {
            for (let key in config.list) {
                list[key] = () => { return config.list[key] };
                //GM_log(key + " : " + config.list[key]);
            }
        }
    }

    if ( document.location.href.indexOf("https://github.com/") == 0 ) {
        GM_setValue(GITHUB_ISSUES_LASTLOADED_URL, document.location.href);
    } else if ( new RegExp(CLOUDJIRA_ISSUE_URL_PATTERN).test(document.location.href) ) {
        GM_setValue(CLOUDJIRA_ISSUES_LASTLOADED_URL, document.location.href);
    } else if (new RegExp(REDMINE_ISSUE_URL_PATTERN).test(document.location.href) ) {
        GM_setValue(REDMINE_ISSUES_LASTLOADED_URL, document.location.href);
    } else if (new RegExp(GITLAB_ISSUE_URL_PATTERN).test(document.location.href) ) {
        GM_setValue(GITLAB_ISSUES_LASTLOADED_URL, document.location.href);
    } else {
        var myAutoCompletionList = {
            "ana"  : () => "Analyzing.",
            "imp"  : () => "Implementing.",
            "test" : () => "Testing.",
            "doc"  : () => "Documenting.",
            "rev"  : () => "Reviewing.",
            "res"  : () => "Researching.",
            "disc" : () => "Discussing.",
            "ref"  : () => "Refactoring.",
            "rep"  : () => "Reproducing.",
            "fix"  : () => "Fixing.",
            // Handling of GitLab, Jira, GitHub and Redmine shortcuts
            "!"    : (arg) => {
                return expandGitLabIssueId("!");
            },
            "@"    : (arg) => {
                return expandCloudJiraIssueId("@");
            },
            "#"    : (arg) => {
                return expandGitHubIssueId("#");
            },
            "$"    : (arg) => {
                return expandRedmineIssueId("$");
            }
        };

        customizeAutoCompletionList(myAutoCompletionList);

        document.addEventListener('keydown', function(event) {
            let sElement = event.target;
            if ( ( sElement.tagName == "INPUT" || sElement.tagName == "TEXTAREA" ) &&
                event.ctrlKey && ( event.key == '.' || event.key == ' ' ) ) {
                expandShortcut(sElement);
                event.stopPropagation();
                event.preventDefault();
            }
        }, true);
    }

    configHelper.addConfigMenu('autocompleter', '{"list" : {"focus" : "Pokus!", "Tili" : "Mili?"}, "defaultProject": "My best project!"}');

    if ( window.location.href.indexOf('https://reports.scand.by/add') == 0
         && document.getElementsByName("rid")[0].value == "0" ) {
        let config = configHelper.getConfigObject(GM_getValue('autocompleter.config'));
        let defualtProject = ( config != null ? config.defaultProject : null );
        let projectSelect = document.getElementById("user-projects");
        for (let i = 0; i < projectSelect.options.length; i++) {
            if ( projectSelect.options[i].text == defualtProject ) {
                projectSelect.options[i].selected = true;
                break;
            }
        }
    }
})();
